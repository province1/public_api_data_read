const axios = require('axios');


async function db_service_request(data, service_url){


  try {  
    var result = await axios({
                    headers: {  'Content-Type': 'application/json'},
                    method: "post",
                    url: service_url,
                    auth: {
                      username: process.env.DB_SERVICE_ID,
                      password: process.env.DB_SERVICE_KEY
                    },
                    data
  })

    return result.data
  }
  catch (e) {
    
    //TODO: Unhandled Error: network error:  1645980232466 { error: true, error_code: 404, error_msg: 'Not Found' }
    
    if( "errno" in e){
      
      // e.errno -111 network connectivity error
      if(e.errno == -111){
        
        console.log("network error: ", Date.now(), {error: true, error_code: 111, error_msg: e.message})
        return {error: true, error_code: 111, error_msg: e.message}
        
      } else {
        
        // New error - not witnessed before
        console.log("network error: ", Date.now(), {error: true, error_code: e.errno, error_msg: e.message})
        return {error: true, error_code: e.errno, error_msg: e.message}
      }
      
    }
    

    if( "response" in e) {
      
      
      // Authorization failure
      if(e.response.status == 401) {
        
        console.log("network authorization error: ", Date.now(), {error: true, error_code: e.response.status, error_msg: e.response.statusText})
        return {error: true, error_code: e.response.status, error_msg: e.response.statusText}
        
      } else {
        
      // New error - not witnessed before
        console.log("network error: ", Date.now(), {error: true, error_code: e.response.status, error_msg: e.response.statusText})
        return {error: true, error_code: e.response.status, error_msg: e.response.statusText}
        
      }
      
      
    }

  }
  
}

  
module.exports = {db_service_request}