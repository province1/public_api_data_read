const { db_service_request } = require("./db_service.js");

async function get_land_data (filter) {
    
    var service_url = process.env.DB_SERVICE_URL + "/current_state_many";
    
    var data = {
        
        collection: "land_data",
        filter,
        grouper: "id"
        
    };
    
    
    var response = await db_service_request(data, service_url);
    
    var clean_response = response.map( x => {
        
        delete x._id;
        delete x.timestamp;
        
        return x 
        
    });
    
    return clean_response
    
    
}


module.exports = {get_land_data}