const express = require('express');
const bodyParser = require("body-parser");
var cors = require('cors');
const helmet = require("helmet");

require('dotenv').config({override: true});

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.use(helmet());

const port = process.env.PORT;

const { db_service_request } = require("./lib/db_service.js");
const { get_land_data } = require("./lib/land_service.js")




app.get('/land_data',async function (req, res) {
    
    var filter = {};
    var response = await get_land_data(filter);
    res.json(response);
    
});


app.post('/land_data',async function (req, res) {
    
    var query_types = ["owner"];
    var filter = null;
    
    if(query_types.includes(req.body.query_type)){
        
        filter = {owner: req.body.query_param};
    } else {
        filter = {};
    }
    
    var response = await get_land_data(filter);
    res.json(response);
    
});


app.get('/equipment_data',async function (req, res) {
    
    var service_url = process.env.DB_SERVICE_URL + "/current_state_many";
    
    var data = {
        
        collection: "equipment",
        filter: {},
        grouper: "serial"
        
    };
    

    var response = await db_service_request(data, service_url);
    
    var clean_response = response.map( x => {
        
        delete x._id;
        delete x.timestamp;
        
        return x; 
        
    });
    
    res.json(clean_response);
    
});

app.listen(port, () => {
  console.log( `${process.env.NAME} listening at http://localhost:${port}`);
});
